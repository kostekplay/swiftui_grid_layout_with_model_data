////  Helloween.swift
//  SwiftUIGridLayoutWithModelData
//
//  Created on 13/11/2020.
//  
//

import Foundation

struct Helloween: Identifiable {
    let id = UUID()
    let name: String
    let image: String
    
    static func addAll() -> [Helloween] {
        
        return [
            Helloween(name: "fire", image: "1"),
            Helloween(name: "scull", image: "2"),
            Helloween(name: "leaf", image: "3"),
            Helloween(name: "hat", image: "4"),
            Helloween(name: "spider", image: "5"),
            Helloween(name: "pumpkin", image: "6"),
            Helloween(name: "sweet", image: "7"),
            Helloween(name: "bat", image: "8"),
            Helloween(name: "cat", image: "9"),
            
        ]
        
    }
}


