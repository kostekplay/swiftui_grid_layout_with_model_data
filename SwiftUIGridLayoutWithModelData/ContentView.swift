////  ContentView.swift
//  SwiftUIGridLayoutWithModelData
//
//  Created on 13/11/2020.
//  
//

import SwiftUI

struct ContentView: View {
    
    let hello = Helloween.addAll()
    
    var body: some View {
        
        let chunkHello = hello.chunked(into: 3)
            
        List{
            
            ForEach(0..<chunkHello.count) { index in
                
                HStack {
                    
                    ForEach(chunkHello[index]) { hello in
                        
                        Image(hello.image)
                            .resizable()
                            .scaledToFit()
                        
                    }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
