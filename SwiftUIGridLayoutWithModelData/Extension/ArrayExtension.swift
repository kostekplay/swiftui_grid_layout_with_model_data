////  ArrayExtension.swift
//  SwiftUIGridLayoutWithModelData
//
//  Created on 13/11/2020.
//  
//

import Foundation

extension Array {
    
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, through: count, by: size).map {
            Array(self[$0..<Swift.min($0 + size, count)])
        }
    }
    
}
